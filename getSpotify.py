import os
from dotenv import load_dotenv, find_dotenv
import spotipy
from spotipy.oauth2 import SpotifyOAuth
from datetime import datetime

load_dotenv(find_dotenv())

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=os.getenv('CLIENT_ID'),
                                               client_secret=os.getenv('CLIENT_SECRET'),
                                               redirect_uri=os.getenv('REDIRECT_URI'),
                                               scope="user-read-recently-played"))

def extract_last_tracks(amount):
    date_today = datetime.today()
    date_unix = int(date_today.timestamp())*100 #pasamos a formato unix
    tracks = sp.current_user_recently_played(limit=amount, after=date_unix)
    tracks_list = []
    
    for track in tracks['items']:
        track_dict = {}

        track_dict['date'] = track['played_at']
        track_dict['name'] = track['track']['name']
        track_dict['url'] = track['track']['external_urls']['spotify']
        track_dict['artist'] = track['track']['artists'][0]['name']
        track_dict['img'] = track['track']['album']['images'][0]['url']
        tracks_list.append(track_dict)
    return tracks_list

if __name__ == "__main__":
    extract_last_tracks(4)
