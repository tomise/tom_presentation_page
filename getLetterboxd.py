import os
from dotenv import load_dotenv, find_dotenv
from requests_html import HTMLSession

load_dotenv(find_dotenv())

S = HTMLSession()

LETTERBOXD_USERNAME = os.getenv('LETTERBOXD_USERNAME')
BASE_URL = f"https://letterboxd.com"
PROFILE_URL = f"{BASE_URL}/{LETTERBOXD_USERNAME}/"

def extract_recent ():
    response = S.get(PROFILE_URL)
    movie_list = response.html.find('section#recent-activity ul.poster-list li')
    return_list = []
    for movie in movie_list:
        movie_dict = {}
        movie_dict['movie_name'] = movie.find('img', first=True).attrs['alt']
        movie_uri = movie.find('div.film-poster', first=True).attrs['data-film-slug']
        movie_dict['movie_url'] = BASE_URL+movie_uri
        movie_dict['movie_poster'] = extract_movie_poster(movie_dict['movie_url'])
        return_list.append(movie_dict)
    return return_list
        
def extract_favourites ():
    session = HTMLSession()
    response = S.get(PROFILE_URL)
    movie_list = response.html.find('section#favourites ul.poster-list li')
    return_list = []
    for movie in movie_list:
        movie_dict = {}
        movie_dict['movie_name'] = movie.find('img', first=True).attrs['alt']
        movie_uri = movie.find('div.film-poster', first=True).attrs['data-film-slug']
        movie_dict['movie_url'] = BASE_URL+movie_uri
        movie_dict['movie_poster'] = extract_movie_poster(movie_dict['movie_url'])
        return_list.append(movie_dict)    
    return return_list
                
def extract_movie_poster (movie_url):
    movie_request = S.get(movie_url)
    poster_html = movie_request.html.find(".poster-list", first=True)
    poster_url = poster_html.find("img", first=True).attrs["src"]
    return poster_url


if __name__ == '__main__':
    extract_favourites()
