from flask import Flask, render_template
import getSpotify
import getLetterboxd

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/tracks")
def tracks():
    return render_template("tracks.html", tracks_list=getSpotify.extract_last_tracks(4))

@app.route("/movies")
def movies():
    return render_template("movies.html", fav_list=getLetterboxd.extract_favourites(), recent_list=getLetterboxd.extract_recent())


if __name__ == "__main__":
    app.run(debug=True, port=3000)
